const { spawnSync } = require('child_process');
const { readFileSync } = require('fs');

const envFileName = process.argv[2];
const sourceMapsPath = process.argv[3];

if (!envFileName) {
  console.error('no environment file name passed as argument');
  process.exit(1);
}

if (!sourceMapsPath) {
  console.error('no source maps path passed as argument');
  process.exit(1);
}

const envFilePath = `./src/environments/${envFileName}`;

let envFile;
try {
  envFile = readFileSync(envFilePath, 'utf8');
} catch(error) {
  console.error(`error reading from ${envFilePath}: ${error}`);
  process.exit(1);
}

let release;
try {
    // the release key must exist in the environment file
    release = envFile.match(/release: '(.+)'/)[1];
} catch(error) {
  console.error(`error searching the release value: ${error}`);
  process.exit(1);
}

const newReleaseCommand = spawnSync(
  './node_modules/.bin/sentry-cli',
  ['releases', 'new', release]
);

if (`${newReleaseCommand.stderr}`) {
  console.error(`stderr trying to create release: ${newReleaseCommand.stderr}`);
  process.exit(1);
}

if (newReleaseCommand.error) {
  console.error(`error trying to create release: ${newReleaseCommand.error}`);
  process.exit(1);
}

const sourceMapsCommand = spawnSync(
  './node_modules/.bin/sentry-cli',
  ['releases', 'files', release, 'upload-sourcemaps', sourceMapsPath]
);

if (`${sourceMapsCommand.stderr}`) {
  console.error(`stderr trying to upload source maps: ${sourceMapsCommand.stderr}`);
  process.exit(1);
}

if (sourceMapsCommand.error) {
  console.error(`error trying to upload source maps: ${sourceMapsCommand.error}`);
  process.exit(1);
}

const finalizeReleaseCommand = spawnSync(
  './node_modules/.bin/sentry-cli',
  ['releases', 'finalize', release]
);

if (`${finalizeReleaseCommand.stderr}`) {
  console.error(`stderr trying to finalize the release: ${finalizeReleaseCommand.stderr}`);
  process.exit(1);
}

if (finalizeReleaseCommand.error) {
  console.error(`error trying to finalize the release: ${finalizeReleaseCommand.error}`);
  process.exit(1);
}
