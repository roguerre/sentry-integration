const { spawnSync } = require('child_process');
const { readFileSync, writeFileSync } = require('fs');

const envFileName = process.argv[2];

if (!envFileName) {
  console.error('no environment file name passed as argument');
  process.exit(1);
}

const envFilePath = `./src/environments/${envFileName}`;

// sentry-cli seems to return the current commit as
// recommended release name
const releaseCommand = spawnSync(
  './node_modules/.bin/sentry-cli',
  ['releases', 'propose-version']
);

if (`${releaseCommand.stderr}`) {
  console.error(`stderr trying to get the recommended release: ${releaseCommand.stderr}`);
  process.exit(1);
}

if (releaseCommand.error) {
  console.error(`error trying to get the recommended release: ${releaseCommand.error}`);
  process.exit(1);
}

// trimming to avoid whitespace from stdout (extrapolating
// because it actually isn't a string)
const release = `${releaseCommand.stdout}`.trim();

// asserting that an environment file exists and
// the `release: ''`, entry exists
let envFile;
try {
  envFile = readFileSync(envFilePath, 'utf8');
} catch (error) {
  console.error(`error reading from ${envFilePath}: ${error}`);
  process.exit(1);
}

const envFileFinal = envFile.replace(/release:.+,/, `release: '${release}',`);

try {
  writeFileSync(envFilePath, envFileFinal);
} catch (error) {
  console.error(`error writing to ${envFilePath}: ${error}`);
  process.exit(1);
}
