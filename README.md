# Sentry Integration With Multiple Releases
This project is a proof of concept for the integration with Sentry
and the implementation of a series of scripts and npm scripts to
automate the sentry releases (uploading the bundled results and source
maps after a build to sentry).